package servlets;

import admin.AccountServer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminServlet extends HttpServlet{

    private final AccountServer accountServer;

    public AdminServlet(final AccountServer accountServer) {
        this.accountServer = accountServer;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(200);
        int usersLimit = accountServer.getUsersLimit();
        response.getWriter().write(String.valueOf(usersLimit));
    }
}
