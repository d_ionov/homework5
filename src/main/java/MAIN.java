import admin.AccountServerController;
import admin.AccountServer;
import admin.AccountServerControllerMBean;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import servlets.AdminServlet;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.logging.Logger;

public class MAIN {

    public static void main(String[] args) throws Exception{

        final org.apache.logging.log4j.Logger logger = LogManager.getLogger(MAIN.class.getName());

        final AccountServer accountServer = new AccountServer();

        AccountServerControllerMBean serverStatistic = new AccountServerController(accountServer);
        MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("ServerManager:type=AccountServerController");
        mBeanServer.registerMBean(serverStatistic, name);

        String portString = args[0];
        int port = Integer.valueOf(portString);

        final Server server = new Server(port);
        final ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(new AdminServlet(accountServer)), "/admin");
        server.setHandler(context);
        server.start();
        logger.info("Server started");
        server.join();

    }

}
