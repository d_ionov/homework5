package admin;

public class AccountServer {

    private int usersLimit = 10;

    public int getUsersLimit() {
        return usersLimit;
    }

    public void setUsersLimit(final int usersLimit) {
        this.usersLimit = usersLimit;
    }

}
