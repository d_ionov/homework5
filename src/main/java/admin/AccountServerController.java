package admin;


public class AccountServerController implements AccountServerControllerMBean{

    private final AccountServer accountServer;

    public AccountServerController(final AccountServer accountServer) {
        this.accountServer = accountServer;
    }

    @Override
    public int getUserLimit() {
        return accountServer.getUsersLimit();
    }

}
