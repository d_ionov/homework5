package servlets;

import admin.AccountServer;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AdminServletTest {

    private HttpServletRequest getMockRequest(){
        return mock(HttpServletRequest.class);
    }

    private HttpServletResponse getMockResponse(StringWriter stringWriter) throws IOException {
        HttpServletResponse response = mock(HttpServletResponse.class);
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(printWriter);
        return response;
    }

    @Test
    void doGetTest() throws IOException, ServletException {
        final StringWriter stringWriter = new StringWriter();
        HttpServletRequest request = getMockRequest();
        HttpServletResponse response = getMockResponse(stringWriter);
        final AccountServer accountServer = new AccountServer();
        AdminServlet adminServlet = new AdminServlet(accountServer);
        adminServlet.doGet(request, response);
        assertEquals("10", stringWriter.toString().trim());
    }

}